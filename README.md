### Requirements
* ruby 2.5.3

### Setup

1. `bundle`
2. `rake db:migrate db:test:prepare`
3. `rake db:seed`

### Tests

Run `rake`
